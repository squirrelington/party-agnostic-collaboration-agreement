# Contribution Nomination Form

Contributions represent elements that if removed from the Art would noticeably change its nature, quality, or integrity. 

I affirm that:
 - all my Contribution nominations for each Collaborator pass the above criterion, 
 - that they are complete according to my observations, 
 - and where I do not know, I do not list the Contribution

 | Type | Description |
| ---- | ----------- |
| Broadcasting | Operation, maintenance, and troubleshooting of streaming encoders, encoding software and network connectivity to deliver robust quality Art live to Audiences. Recording during live streams. Recording outside of a live stream is credited to *Recording*. |
| Coaching | Facilitation and guidance toward the perfection of an ensemble's practice and artistry. |
| Communiteering | Moderation, cultivation, and retention of passionate and non-toxic Audience members. |
| Design | Creation of graphics, photos, costumes, props, lighting, or set dressing, creation of Show sequences and the Show format, creation of graphic designs for merchandise and marketing materials. |
| Development | Providing custom made-to-purpose technical solutions and tools to aid other Collaborators, or providing hardware or software to connect with and engage the Audience in the Art. |
| Direction | Initial Art conception; facilitating research in preparation for creating the Art; directing practice toward the Art concept. |
| Editing | Live switching of camera feeds, post-editing, adjustments to recorded assets, effects, and post-processing. |
| Engineering | Maintenance, troubleshooting, and coordination of the safe setup, teardown, and application of all tooling and equipment. |
| General Contribution | Credited only in the circumstance that a Collaborator is not credited any other Contributions. |
| Generative AI | Credit for the allowance by a Collaborator for the use of their voice, image, intellectual property including copyright, or identifiable artistic style by deep neural network algorithms to the end of creating additional Contribution elements or revisions beyond audio-visual enhancement effects, audio mastery, and color correction. |
| Marketing | Planning, coordination, and execution of Audience outreach and calls to action. |
| Music | Composition or improvisation of pre-recorded or live music. |
| Operation | Artistic or Cinematographic use of cameras, lights, sound, effects equipment, or specially-designed equipment created by *Development* during a live stream. Operation outside of a live stream is credited to *Recording*. |
| Organization | Scheduling and coordinating Shows and Collaborators in Shows. |
| Performance | Allowing yourself, for the purpose of contributing to the Art, to be perceived by the Audience or by active recording equipment whose recordings are presented to the Audience. This definition is left intentionally broad, and includes the requirement of the Collaborator’s express intent to perform so that artistic experimentation remains unlimited. |
| Recording | Photography, Cinematography, Sound Recording for post-processing of recordings to be assets in Art. Recording of Art for live streams is credited to *Broadcasting*. Photography, Cinematography, and Sound recording during a live stream is credited to *Operation*. |
| Venue | Preparing a recording/presentation space to meet the needs of Art creation, such as electrical, internet, acoustics, and rigging. |
| Writing | Words or stage directions that other Collaborators say or carry out. |


```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```

```
Collaborator Name: [                              ]
I Observed them Contributing:


I am certain they did not Contribute:


```
