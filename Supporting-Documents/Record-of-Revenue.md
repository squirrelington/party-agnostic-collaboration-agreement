# Record of Revenue

This is a quick starting point for recording Payments you receive for the Art, and Payouts that are due/fulilled.
It will almost certainly get unwieldy if you are a Revenue Administrator for several successful SPACAs.
There are far more sophisticated ways to handle this.

SPACA Name:__________________________________

### Broadcast Record (Live Streams):
Broadcast Platform:__________________________ <br>
Channel:_____________________________________ <br>
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

<br>
<br>

Broadcast Platform:__________________________ <br>
Channel:_____________________________________ <br>
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

(add more lines as needed)

### Free-to-Watch Collections (like YouTube)
Video on Demand Platform:____________________ <br>
Channel:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Art Attributed Revenue:______________________ <br>
Unattributed Revenue:________________________ <br>
Total Channel Views:_________________________ <br>
Art Video Views:_____________________________ <br>
Total Revenue:_______________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

<br>
<br>

Video on Demand Platform:____________________ <br>
Channel:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Art Attributed Revenue:______________________ <br>
Unattributed Revenue:________________________ <br>
Total Channel Views:_________________________ <br>
Art Video Views:_____________________________ <br>
Total Revenue:_______________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

(add more lines as needed)

### Subcription Collections (Like Vimeo)
Subsription Platform:________________________ <br>
Channel:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Subscription Revenue:________________________ <br>
Total Channel Views:_________________________ <br>
Art Video Views:_____________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

<br>
<br>

Subsription Platform:________________________ <br>
Channel:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Subscription Revenue:________________________ <br>
Total Channel Views:_________________________ <br>
Art Video Views:_____________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

(add more lines as needed)

### Other Donation Platforms (like Ko-fi)
Note: only record here for Donation Platforms not linked in one of the above Platforms

Platform:____________________________________ <br>
Account:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

<br>
<br>

Platform:____________________________________ <br>
Account:_____________________________________ <br>
Period of Record
Date and Time Start:_________________________ <br>
Date and Time End:___________________________ <br>
Revenue:_____________________________________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

(add more lines as needed)

### Revenue from Merchandise
Date and Time:_______________________________ <br>
Items & Quantities Sold:_____________________ <br>
Total Charge:________________________________ <br>
Costs - Manufacturing & Materials:___________ <br>
Fees - Shipping & Tax:_______________________ <br>
Total Revenue After Costs & Fees:____________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

<br>
<br>

Date and Time:_______________________________ <br>
Items & Quantities Sold:_____________________ <br>
Total Charge:________________________________ <br>
Costs - Manufacturing & Materials:___________ <br>
Fees - Shipping & Tax:_______________________ <br>
Total Revenue After Costs & Fees:____________ <br>
| Collaborator: | Revenue due: | date paid: |
| ------------- | ------------ | ---------- |

(add more lines as needed)