# Reserved Intelletual Property List

The list below represents my declared list of intellectual property, including copyright reserved against use in the SPACA. I assert that I will refrain from using or referencing in any way the stated Intellectual Property in my Contributions to the Art.

Collaborator Name:_____________________________________ <br>
SPACA Name:____________________________________________ <br>
Time and Date:_________________________________________

List of Reserved Intellectual Property Rights: