# Record of Governance
This Document is a helpful tool that aggregates when 
 - Art was Completed, Contribution List Certified, SPACA Finalized, etc
 - Votes were called for and their results
 - Approvals, Second Approvals, Overrides of those Second Approvals, etc
 - Statements declared by Collaborators
 - When actions were taken, when conditions were in effect for reporting and record-keeping reasons.
it is not officially part of the SPACA, but can serve as Evidence.

SPACA Name:_________________________________________ <br>

<br>
<br>

Date and Time:______________________________________ <br>
Action taken:_______________________________________ <br>
Result of Action:___________________________________ <br>
Result Effective Date and time:_____________________ <br>

<br>
<br>

Date and Time:______________________________________ <br>
Action taken:_______________________________________ <br>
Result of Action:___________________________________ <br>
Result Effective Date and time:_____________________ <br>

<br>
<br>

Date and Time:______________________________________ <br>
Action taken:_______________________________________ <br>
Result of Action:___________________________________ <br>
Result Effective Date and time:_____________________ <br>

(Add more as needed)