# Revenue Distribution Receipt
This is a recept of Distribution reporting Payout

From Revenue Aministrator:______________________ <br>
To Collaborator:________________________________ <br>
Regarding SPACA Name:___________________________

Period of Revenue Collection: <br>
Date and Time __________________________________ <br>
to <br>
Date and Time __________________________________

Revenue During Period <br>
From Broadcast:__________________________________<br>
From Video on Demand:____________________________<br>
From Subscriptions:______________________________<br>
From Donations:__________________________________<br>
From Merchandise:________________________________<br>
From all other sources:__________________________<br>
Total Revenue Collected:_________________________<br>
Collaborator Percentage of Revenue:______________

Amount Paid Out:_________________________________