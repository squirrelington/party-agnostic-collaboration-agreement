# Boundaries List

The list below represents my stated list of boundaries beyond General Boundaries found in the Squirrelington Working Agreement. I assert that these boundaries represent conditions where personal, psychological, physical or spiritual harm will come to me, and are not simply preferences or comfort zones. Boundaries may be amended ore removed. If the Boundaries stated in one list conflict with another, the boundaries dated last carries from that date forward.

Artist Name:_____________________________________ <br>
Working Agreement Location:______________________ <br>
from datetime: __________ to datetime: __________

List of Boundaries: