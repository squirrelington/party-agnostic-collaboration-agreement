# Squirrelington Working Agreement
## Version 2.0.0 - February 7, 2024

## **Summary:**
This Agreement is to set safe and fair working conditions with opportunities for growth and discovery for a group of equal self-organizing artists each contributing their skills to create a work of art as a collective with no centralized authority.

## Scope
This Agreement pertains to any Art created by the signatories below created in sessions at the location:_________________________________________________________________________________________________________________________________________________ 
from datetime: __________ to datetime: __________.

#### **Drafters’ Indemnity:** 
This document is presented as-is and is not encouraged nor discouraged for use by its writers. It is agreed by all signatories of this document that any consequences to the signatories, or loopholes in the conditions are the fault of the writers of this document. Such writers are held blameless. All signatories agree to this document at their own risk and irrevocably waive any and all claims, of any kind or nature whatsoever, against any and all writer(s) of this document.

## Definitions

**Artist:** An Artist is any individual that brings an artistic, organizational, strategic, and/or technical Contribution to the Art defined below.

Artists agree to attempt to deepen their areas of expertise in the course of creating Art under this working agreement. Artists also agree to practice in more than a singular domain of expertise and grow their skills in new domains to benefit their own personal self-reliance, to benefit this and future Ensembles, and to benefit the general social good of humanity in creating Art. Artists also agree to coach and mentor other Artists in their areas of expertise as opportunities arise. All signatories of this document are defined as Artists

**Ensemble:** The collective of all Artists who have signed identical agreements of the same Scope.

**Art** Art is the product that this Ensemble Creates. It may be in one piece or in several component pieces, and may have versions, and iterations. How that Art is owned, stored, distributed, and managed is outside the scope of this agreement.

**Artefacts** In the creation of Art, there will be component pieces, including, but not limited to raw recordings, intermediate elements, and plans, written or coded. Such things are called Artefacts. How Artefacts are stored, or managed is outside the scope of this agreement.

**Packaging** Products created for the purpose of promoting, or advertising the Art.

**Outside Ensemble:** A Ensemble formed under an identical or similar agreement to this one. Such a Ensemble may have some or all of the same Artists as in this agreement but is considered a discrete entity under a discrete agreement that is in no way bound to this one.

**Boundaries List:** or “List of Boundaries”, is a separate document declaring an Artist’s disallowed themes, words, physical expressions and touches. A Boundaries List may accompany this agreement for each Artist who requires such boundaries to be honored by the Ensemble. A boundary in this context defines a condition that is expressly forbidden by the Ensemble to act out, speak, or perform in the presence of the Artist attaching the list. In good faith, boundaries represent physical and psychological limits beyond which the Artist is not safe or in control of themself. A Boundaries List is not a list of discomforts, dislikes, or disinclinations. The aforementioned are examples of comfort, and the boundaries those impose are “comfort boundaries”. The intent of a Boundaries List is to enforce safety boundaries and not comfort boundaries. Comfort boundaries should be pushed. Safety boundaries must never be pushed. A Boundaries List is a list for marking the limits beyond safety where physical and/or psychological injury will occur.

## **Working Agreement**

### **Artist Internal Responsibilities:**

Each Artist holds themself to all of the individual responsibilities and agreements required of all other Artists described herein.

**Collaborative Self-Organization:** The intent of this working agreement is to create the bounds by which the Ensemble will work together as equals to create a work of Art. All Artists hereby assert their agreement that this work is a collaborative engagement and there is no one person, entity, or group except the Ensemble itself who issues commands, commissions work, fully owns the artistic vision, or has artistic control. All Artists further assert they understand that in the absence of any said centralized authority, they will work together respectfully to create Art that will potentially surprise all involved, and that the Ensemble will self-organize to safely achieve high-quality, artistically satisfying, Art at speed for the collective benefit of all Artists.

**Liability Insurance:** Each Artist will maintain their own liability insurance for damages to property or rental property, theft, negligence, and physical injury. Any damages caused will be at the Artist’s risk and not the Ensemble.

**Safety Focus and Adaptation to Needs:** Each Artist will act responsibly to watch for and help mitigate physical and psychological safety concerns, support all other Artists' needs to maintain and improve their own physical and psychological wellness, and adapt their internal processes to work safely with the entire Ensemble.

Each Artist will watch for opportunities to continuously improve their safety practices in the interest of creating a faster, higher quality, and more facile collaborative creative environment. This improved environment will create greater physical and psychological stability for all Artists. This stability will allow for greater artistic risk-taking and innovation, pushing the boundaries of Ensemble’s comfort with as little risk to physical or psychological safety as possible.

**Share Expertise and Learn:** Each Artist agrees to share their experience, technical knowledge, and artistic techniques to other Artists through coaching and mentorship whenever the opportunity presents itself and as time allows to the end that the entire Ensemble becomes more cross-trained and capable.

Each Artist is expected to aggressively listen to and learn from their fellow Artists regardless of personal feelings about the skill or expertise’s worth, and regardless of personal feelings concerning their own aptitude. Artists will fearlessly investigate, come to understand, and practice skills outside their current expertise and comfort. Artists are expected to conduct this aggressive listening and investigation within the boundaries of physical and psychological safety defined herein and within all attached Boundaries Lists.

**Mutual Respect:** Each Artist will likewise treat all other Artists with dignity and respect, honoring and asking for consent. Each Artist will watch for and report emergency incidents of micro-aggression, intent versus impact, sexual and other forms of harassment, discrimination, and related legally protected civil rights violations. If such an incident is observed, that observation is to be reported to the Safety Officers described in this agreement.

**Emergency Preparedness:** Each Artist, or Company of Individual Artists, will be responsible for its own healthcare and health insurance. The Ensemble works in industrial and uncontrolled environments around heavy professional equipment being used in novel artistic ways. It is incumbent on the Ensemble to have a first response emergency plan in place for accidents and injuries. In the event of an emergency where physical injury occurs, the injured may be offered first aid by other Artists. By accepting the offered first aid, the injured party holds all parties administering first aid blameless for injuries incurred or worsened in the course of administering the first aid.

In the event the injured is unable to consent to first aid, by signing this document, the injured agrees to the sincere efforts to lessen their injury by all parties present and holds blameless any such parties from further injury or malpractice in the absence of a trained administer of first aid, or a 911 call operator.

**Physical Safety:** Each Artist will maintain strict safety standards around staged intimacy, combat and stunts, and waives all claims against and holds all other Artists harmless and blameless for injuries caused in the practice of well-rehearsed safety-focused staged intimacy, combat, or stunts and will fully indemnify all other Artists against any and all such claims or awards.

Each Artist will watch for tripping hazards, falling hazards, and hazards caused by unsecured equipment. When such a hazard is discovered, other work must stop until the hazard is secured or mitigated. A performance or practice must not begin until all equipment present is secured from hazards.

Each Artist must treat studio and streaming equipment with extreme care and situational awareness. No Artist will allow themselves to be present with equipment in an impaired mental or physical state as what would constitute Driving Under the Influence. 

In the event of an emergency where it becomes clear that an Artist will either damage equipment or cause significant injury to themselves or others, and it is within the Artist’s control to choose, that Artist must choose to damage the equipment. Each Artist will hold blameless and harmless and waives all claims of any nature against other Artists and will fully indemnify all other Artists against any and all such claims or awards for damage to or destruction of equipment in the sincere pursuit of creating Art.

**Psychological Safety and General Boundaries:** Under no conditions will an Artist completely undress, nor show any body part considered indecent by the United States Federal Communications Commission or disallowed by Twitch or Youtube’s respective terms of service. Under no circumstances will the physical touching of bathing suit areas be allowed during practice or during Art creation.

The photographic suggestion of nudity without requiring an Artist to undress beyond swimming pool decency, and the indication of nudity or intimacy by narrative, dialogue or space-work is allowed as long as the Artist consents prior to doing so. In the same manner, suggestions of intimate touching are allowed by suggestive photography, narrative, dialogue, or space-work provided all Artists involved consent before-hand, and no actual touching is done.

*The definition of space-work:* Space-work, sometimes called mime, pantomime, or object-work, is the suggestion of touching or interacting with an object without that object being present, or without that object being actually in contact with the Artist’s hands or body. For example, an Artist might hold a space-work balloon without needing a real balloon or cradle a space-work baby without requiring a living child or a prop of swaddling clothes or ride a space-work horse without needing to care for, be safe around, or even know how to ride a real horse. Space-work has been used in improv frequently where there is no way to know ahead of time what props will be needed. It also can provide safety when the narrative calls for firearms, stage combat, intimacy, or stunts.

Uncouth language is generally permitted whenever it serves the narrative; however, words, themes, and situations that are mentioned in an Artist’s attached personal Boundaries List are expressly forbidden. Racial slurs are never permitted to be spoken except in the case where the Artist is of the race that slur is meant to target, the aforementioned Artist personally consents to say such words, and such words are not listed on any Artist’s attached Boundaries List. No Artist will force another to go beyond the limits of their predefined List of Boundaries in the interest of protecting physical and psychological safety.

### **Ensemble Roles and Practices:**

**Engineer in Charge:** or EIC, is chiefly responsible for making sure all equipment is well-cared-for and functional. They are responsible for directing Equipment Setup and Teardown. They are also responsible for troubleshooting any technical failures or operating errors of that equipment. Finally, they are responsible for making sure all equipment is appropriately stored and packed for transport to each and every Artist owning or renting studio or streaming equipment. The EIC is expected to have a deep understanding of all studio and streaming equipment in use, and how those pieces of equipment interact to provide a robust and versatile artistic tool for the Ensemble. The EIC is also expected to share knowledge and to mentor other Artists to be EICs in their own right.

**Director:** is chiefly responsible for helping the Ensemble define the Ensemble’s show concept, for guiding the Ensemble towards the Ensemble’s show concept, and for helping to shape the expression of the Ensemble’s show design assets, marketing, and merchandising. The Director expressly does not exercise artistic control, instead acting as a guide for the Ensemble to find its own show. The reason the Director has a modified role in this agreement as opposed to traditional theater or video media is because this is an equal Ensemble, all Artists come as they are bringing what they will, discovering the Art together. Following someone else’s every command without added compensation is not fun, nor does it offer the artistic expression and freedom this agreement is designed to support. Hence, Directors are defined in this agreement as guides to the Ensemble, not commanders.

Unless the Ensemble verbally agrees beforehand to have a sole director, at the Ensemble’s discretion, a different Artist may be elected to Direct during any given practice session.

**Coach:** is chiefly responsible for the development of the Artists’ individual artistic talents and techniques, along with the development of the combined artistic capabilities of the Ensemble. A Coach’s recommendations are to be taken as guidance, not commands. The Coach and the Director may be the same Artist or may work together to help draw out the talents and abilities of the Ensemble in the Products produced.

Unless the Ensemble verbally agrees beforehand to have a sole Coach, at the Ensemble’s discretion, a different Artist may be elected to Coach during any given practice session.


**Technical Safety Officer:** or TSO, is chief in charge of making sure physical safety practices are adhered to. These include safety around equipment and safety in physical performance by Artists performing and Artists operating moving equipment, such as cameras. The TSO cannot also be the RSO defined below. The TSO must understand the individual responsibilities described above intimately and have technical experience sufficient to spot physical safety concerns in equipment hazards as well as staged intimacy, combat and stunts. The TSO will be aware of where emergency and first aid kits are located, familiar with the use of said kits, and prepared to invoke any emergency safety procedures, such as fire suppression and application of necessary first aid. All Artists agree to follow all of the TSO’s safety directions immediately. The TSO is likewise responsible for enacting any and all Covid regulations and safety practices.

**Relational Safety Officer:** or RSO, is chief in charge of making sure psychological safety practices and boundaries are adhered to. The RSO cannot also be the TSO. The RSO is expected to be aware of the general and specific boundaries of the Ensemble, have generally accepted good communication, leadership, or mediation skills, and to be confident in their own skills in those regards. The RSO will facilitate respectful awareness of those boundaries as needed during practice, warmups, and artfully during performance. The RSO will be present to mediate or communicate between Artists who fear that safety boundaries are being pushed. The RSO is also responsible for facilitating, or delegating facilitation of the Retrospective defined below. The RSO will carry out their role of maintaining psychological safety so that the Ensemble may push themselves past their comfort boundaries to create engaging quality Art. All Artists agree to follow all of the RSO’s safety directions immediately.

**Andon Cord:** Safety and quality are the concern of every Artist, not just the Safety Officers. This is expressed above in the Artist Internal Responsibilities section. The Andon Cord is an additional practice in which any member of the Ensemble may stop all current work preparing or practicing for Product creation to get the entire Collective to work on a safety or quality concern. All Artists are expected to “pull the Andon Cord” when they see such a concern. The cord is “pulled” by an Artist saying “Andon”, or simply “Everyone stop!”. At which point the Ensemble sets down what they are doing, circles up to see what the problem is, and addresses it. Once it has been addressed, the Ensemble returns to their original work. The Andon Cord may be pulled for a physical safety concern with staged combat or loose cabling, or for a psychological safety boundary issue, or for a quality concern. The problem should take no more than five minutes to address. A potential concern should never seem to be too trivial to circle the entire Ensemble over the issue. And the Artist who pulls the cord over a “trivial” concern should not be criticized for it. If the problem can be solved quickly by one of the other Artists, then it should be solved quickly. With the extra time, the Artist who knows is expected to coach the Artist who does not know. If it appears too complex to solve in five minutes, it is noted and brought up as a topic in the Retrospective described below.

**Retrospective:** The retrospective is an opportunity for the Ensemble to reflect on its work. It is essential that the Retrospective be conducted in a blameless manner, where all Artists are free from blame, do not cast blame, and are psychologically safe to explore ways in which the collective might improve its manner of working. It is the RSO’s responsibility to frame and facilitate this discussion. The RSO may elect to delegate their Retrospective responsibilities to another Artist if both parties consent.

During the Retrospective, the Ensemble notes observations on things that went well, things that could be improved, and things that were confusing. Once those are identified, the Ensemble works to find the root cause of those observations. Once a root cause is satisfactorily found, the Ensemble notes the root cause and proposes an experiment for the next Ensemble.

In order to expedite this process for use during the 15 minutes of Last Looks, The RSO or their chosen delegate will select an Artist at Random once for each question: What went well, what could have gone better, and what was confusing. The Ensemble will look for a satisfactory root cause for each and come up with no more than three experiments to try in the next Ensemble. Those proposed experiments will be attached to the next working agreement and considered during the next Retrospective.

This process will continue from Ensemble to Ensemble, each one contributing to the effectiveness of all future Ensembles.

**Post-Mortem Discussion for Safety Incidents:** In the event an incident occurs where any of the above emergency procedures are invoked or are disregarded, the Ensemble agrees to a post-mortem discussion on how the incident was allowed to happen, and an action plan created to prevent a second occurrence. This post-mortem discussion will be scheduled separately by the Safety Officers on duty, or as part of a practice session.

**Safety Officer Instructions, Andon Cord:** All such directives are to be given respectfully, but firmly. Such directions are to be given assuming best intentions, except in the case of clear and flagrant violations of safety. Such Directives are to be given blamelessly by the Artist issuing the direction. Except in clear and flagrant violations of psychological safety, the Artist receiving the direction accepts that such a direction is a safety and/or quality concern and not a personal attack.

**Start-With-Best-Intent and Misstep Procedures:** In the event that any Artist neglects, is perceived to neglect, violates, or is perceived to violate the above individual responsibilities, such an Artist is considered a misstepping Artist. Such a misstepping Artist, any Artists that witness the misstep, and any Artists that are personally affected by the misstep agree to first take the misstep to the Safety Officers who will record the incident along with any mediation and agreements reached. All parties concerned agree to mediate then arbitrate before taking other Artists to court, allowing opportunities for good faith self-improvement as long as the situation is not in clear and flagrant disregard of the working agreement.

**Clear and Flagrant Disregard for the Working Agreement:** In the event that an Artist clearly and flagrantly disregards the Working Agreement, that Artist is summarily Ejected from the Ensemble.

If such a condition occurs, the two Safety Officers on duty, or in the case of a Safety Officer violating this agreement, the other Safety Officer and a witnessing Artist, will mark the Artist as “Ejected” on this document along with each of their signatures. A co-written and co-signed incident report with witness statements of the nature of the clear and flagrant violation will accompany this agreement.

In extreme cases where law is also violated, a police report will be filed, and the violating Artist will be prosecuted to the full extent of the law.

### **Forming a Ensemble:** 

A Ensemble is formed upon all Artists signing an Agreement in the same version, in the same scope, with the same wording. Each Artist must sign a Working Agreement before the beginning of collaborative work.

In signing, Artists must either affirm in the agreement that they accept the general boundaries defined herein, or in addition to those general boundaries, attach a Boundaries List of additional prohibitions they require the Ensemble to adhere to.

By signing the agreement, all Artists agree to conduct themselves according to the above-mentioned Internal Responsibilities. They likewise affirm that all other Artists are willing and able to maintain their own internal responsibilities. In the event that a given Artist is certain that another Artist cannot or will not honor the above-mentioned internal responsibilities, that given Artist has the responsibility to Disengage from the Ensemble, which is defined in the section on Exiting this Agreement.

Amending a Boundaries List: Each Artist has the right to change its boundaries whenever it so desires, and must attach a dated replacement Boundaries List to the agreement to indicate the new boundaries. Artists making changes must notify the rest of the Ensemble of the amendment by speech, text, or whatever convenient reasonable means of communication is available.

It is also an Artist’s responsibility to be watchful of changes made to the Boundaries Lists of their colleagues. Every Artist agrees to respect the current boundaries listed.

**Amending a Boundaries List:** Each Artist has the right to change its boundaries whenever it so desires, and must attach a dated replacement Boundaries List to the agreement to indicate the new boundaries. Artists making changes must notify the rest of the Ensemble of the amendment by speech, text, or whatever convenient reasonable means of communication is available.

It is also an Artist’s responsibility to be watchful of changes made to the Boundaries Lists of their colleagues. Every Artist agrees to respect the current boundaries listed.

### **Art Creation:**

**Safety Review:** Upon Signing the agreement, if there are any Artists present where this is their first agreement signed, or there are any changes in the Boundary Lists, Five minutes will be spent at the top of the Ensembles meeting to review the agreement, safety, emergency plans, and the Boundaries Lists.

Any additional working agreements deemed necessary by the Ensemble are proposed, assented to, recorded, and attached to the agreement.

The EIC, TSO, and RSO are elected by the Ensemble under the condition that only candidates meriting the responsibility are volunteered.

At the discretion of the Ensemble, a Director and/or a Coach may be elected. In the instance of a practice session, Directors and Coaches take an active role in facilitating the practice. In the instance of a live streamed show or a recording, Directors and Coaches only take notes to give after the performance. Such notes will be given at the end of the session, asynchronously, or at a separately scheduled meeting at the Ensemble’s discretion.

**Equipment Setup:** Once The Agreement is completed and reviewed and safety officers are elected, The Ensemble will set up the equipment needed to stream.

All Artists are expected to help with safe equipment setup and teardown according to their ability. All Artists who are able to set up and teardown equipment are expected to mentor and coach less experienced Artists in becoming better able to independently set up and teardown all equipment as time allows.

Once the equipment is set up securely with all tripping, dropping, and falling hazards satisfactorily addressed, and with all necessary functional tests passing, the Ensemble will begin physical and artistic warmups.

**Framing Packaging:** During the setup, teardown, and/or during any show intermissions, there may be live or pre-recorded Framing Packaging that may involve, but may not be not limited to, commentary, interviews with Artists, performance of short bits, promoting or celebrating Artists or Outside Ensembles, and calls to action to viewers asking them to give tips, purchase tickets, subscriptions, and merchandise.

For the purposes of audience engagement, and Revenue generation among other benefits, it is necessary for Artists to agree to support and participate in such Framing Packaging. Such Framing Packaging may also be used by Artists to create Marketing Segments for advertising and social media platforms, or other Packaging.

**Practice or Performance:** After warmups, a Show or a practice session will begin. The majority of the meeting should be spent in this time. In cases where the Show has a live audience, the space will be opened to that audience first. Efforts will be made to ensure their safety and get their agreement to the recording. Such an agreement will be attached separately to this agreement.

**Intermission:** There may be intermissions during which time there may be Framing Packaging broadcast. Intermissions shall not number more than one for every 22 minutes of show time. An intermission should not last longer than 10 minutes. Typically, a 90-minute show will have one intermission.

**Equipment Teardown:** Upon completion of practice or Art creation, and upon the exit of any audience present, the Ensemble will safely deconstruct and store, or store for transport, if necessary, all the equipment set up earlier. This practice is called Teardown.

**Last Looks:** Once Equipment is safely stored, the Ensemble will conduct a physical “cool down” while having a post-practice retrospective described below and record the Contributions of the Artists in a Contributions List that will accompany this document, and any recordings. It should all take no more than 15 minutes. Upon completion, the Ensemble conducts an equipment “Last Looks”, where they search for any equipment out of place or left behind before disbanding.

#### **Continual Art:**

Whenever A Ensemble’s Art ends when an Outside Ensemble begins, or a Ensemble begins when an Outside Ensemble ends, with less than one hour interruption between works of Art, there is Continual Art.

**Interstitial Framing Packaging:** In the interest of retaining the Audience’s attention between streams, when there is a time gap of less than one hour between one Art ending and another Art beginning, Framing Packaging may be presented to bridge that gap. Streaming Framing Packaging is the responsibility of the Ensemble that is about to begin its Art. The beginning Ensemble must sign their Ensemble Agreement and conduct a Safety Review before they can begin streaming or recording their required Interstitial Framing Packaging. They must also allow themselves enough time to safely set up any studio or streaming equipment necessary for their Interstitial Framing Packaging. It is a best practice for the ending Ensemble to have emergency Framing Packaging on standby in case the beginning Ensemble misses the handoff. In the event the starting Ensemble misses their handoff, a post-mortem is to be conducted to determine how such a miss can be prevented in the future.

**Same Equipment Handoff:** In the event that the same Studio Equipment is used between Ensembles, the ending Ensemble does not conduct an Equipment Teardown. A briefing and handoff between certain key operators must be successfully executed. All Cameras are locked down and safely secured in sensible angles. The Live Editor will leave the camera switcher on the Main Camera or on one designated for Interstitial Framing Packaging. The Broadcaster will cut to any pre-recorded Interstitial Framing Packaging, to Live Interstitial Framing Packaging, or to Lobby Graphics, then give a brief report of any unusual conditions they have noticed to the incoming Broadcaster who is taking over. Outgoing safety officers will give a brief report to Incoming ones, and The ending EIC will give a report on troubleshooting or maintenance issues to the beginning EIC. The ending EIC, the Safety Officers, and the Broadcaster will remain on standby for five minutes as their counterparts take over to ensure a smooth transition, stepping in with context as needed. The Ending Ensemble performs all but the equipment-centric elements of Last Looks in a separate room or space as the beginning Ensemble continues their Art or Interstitial Framing Packaging. If the Ensemble ending is the last Ensemble of the Continual Art, they will perform the Equipment Teardown, Post-show Framing Packaging, and Last Looks.

**Remote Studio Handoff:** In the event that a different piece of equipment is streaming the beginning Ensemble’s Art or Interstitial Framing Packaging, whether those pieces of equipment are across the room or across the planet, the Broadcasters of each Ensemble will work together to coordinate the handoff, ending one stream and beginning the next with minimal interruption. The Ending Broadcaster will be prepared with continual or looping Interstitial Framing Packaging in the event the beginning Broadcaster is having technical difficulties. Broadcasters across Ensembles are expected to help each other troubleshoot and improve.

**Working Agreements Across Ensembles:** Every Artist in this Ensemble is expected to conduct themselves according to this working agreement when interacting or coordinating with Artists of Outside Ensembles. It is understood that working agreements may differ in important details between Ensembles. If there is a misstep or a perceived clear and flagrant disregard for the working agreement observed, the observing Artist is to report their observations to each Ensemble’s RSO. The two RSOs, or in the event the external organization is operating under an agreement without an RSO, whoever is in charge, are to mediate the dispute. All Artists agree to this course of action before moving to arbitration and then to court proceedings. It is understood that a clear and flagrant disregard of the working agreement, even to an Artist of a different Ensemble or external organization, is grounds for Ejection from the Ensemble.

### **Packaging Creation:**

The above physical safety standards will be observed whenever there is the potential for staged intimacy, combat, or stunts, and where studio or streaming equipment is present. A Safety Review, along with Setup and Teardown Procedures will be conducted whenever Studio or Streaming equipment is required. All psychological safety standards will likewise be observed whenever two or more Artists communicate, share work, or are present in the same workspace.

### **Agreements on Supporting Other Artist’s Art:**

Cross-Promotion is Encouraged: Each Artist agrees to find and take opportunities to promote the others in the Ensemble through their own Platforms. By promoting each other, we can concentrate, share and grow our audiences together and contribute to an artistic movement we are all a part of.

**Never Share Personal Information:** For safety, under no circumstances shall any Artist share personal or private details of other Artists. Artists will also watch for elements in the background of photos or videos that may give potential stalkers a clue as to the whereabouts of the Artists in those promotional materials.

**Delivery of needed Elements:** Each Artist agrees to deliver needed Audio, Video, Photographic, Graphic, and Text Copy to Designing Artists, or their commissioned agents, by any deadline requested, so long as that deadline and the scope of the work requested is reasonable. These elements are for assets of the Art, and for Marketing Segments or still graphics. Artists agree to let any Designing Artist know as soon as is reasonably possible if they cannot start to meet, will miss, or are likely to miss the deadline to give the Designing Artist as much time as possible to create an alternative. Artists agree to only deliver elements that they would like to see used in assets and Marketing Segments.

**Give Opportunities to Edit:** Each Artist will share with the Ensemble any Marketing Segments or promotional materials at least 24 hours before posting on their platform to give each other a chance to ask for amendments or changes. This is also to lessen Quality Stop requests.

**Assume Respect and Appreciation:** If an Artist respectfully asks to have a promotion taken down entirely or modified, The posting Artist will take their request courteously and with respect, understanding that every Artist appreciates the effort and intent to promote the others in the Ensemble.

**Allowances and Limitations of Private Help:** It is expected that Artists will give each other private encouragement, feedback, mentorship, and coaching as time and circumstances allow. It is also understood that accepting or acting on any such help by Artists other than Directors, Coaches, Safety Officers, or the EIC in their given domains of responsibility is at the receiving Artist’s sole discretion. It is also understood that if an Artist asks another Artist to stop giving such help, that Artist will respectfully stop in honor of the receiving Artist’s own artistic journey. An Artist should never have to ask a fellow Artist to stop twice.

**Respect for Private Conversations Held in Trust:** Private conversations held in practice, private mentoring and coaching, private group discussion of process or boundaries, and similar moments held in trust are not to be shared without the express permission of all Artists involved.

**Abuse of Trust must be Reported:** While respectful private conversations held in trust should be confidential, if the private conversation violates physical or psychological safety, or if a private conversation is made public without the consent of all Artists involved, that incident must be reported to the RSO, or to the TSO and EIC if the violation is by the RSO. In the event of such an apparent violation, the Safety Officer will record the incident along with any mediating steps and agreements reached. All parties agree to mediation, then arbitration before going to court. If the violation is a clear and flagrant breach of the working agreement, the violating Artist will be Ejected from the Ensemble as stipulated above.

### **Agreements on sharing and interacting with Audiences:**

**Audience is Critical:** The outreach, encouragement, engagement, and retention of the Ensemble’s Audience is as critical a function to the creation of Art as the Art itself. All Artists agree to treat the Ensemble’s collective Audience with the utmost delight, creating positive relationships, and a community of joyful, enriched, non-toxic, psychologically safe supporters of the Ensemble’s Products, and supporters of the Artists as Artists.

**Artistic Movement:** All Artists agree to offer to the audience inclusion in an artistic movement of new art including, but not limited to, narrative improv and new plays.

**Role Modeling:** All Artists agree to role modeling that makes Friendship, Teamwork, and Listening cool and copyable by our audience.

**Six Values:** All Artists further agree to frame interactions with audiences that support and model the values of 
  - Courage, 
  - Joy, 
  - Optimism, 
  - Inclusion, 
  - Connection, and 
  - Empathy, 
especially when the Art itself is emotionally challenging. Artists agree to approach Audience interactions with these six values in mind.

**Never Willfully Lose the Audience:** All Artists agree, regardless of the audience’s demographics, of opinions expressed, or of political beliefs to never disregard, neglect, abandon, or send away the Ensemble’s Audience excepting in cases of individual bad faith Audience members defined in Boundaries for the Audience listed below.

**Boundaries for the Audience:** Regardless of the level of support, no audience member is allowed to know information held in trust by the Ensemble, nor any personal private information including but not limited to home addresses, private phone numbers, credit card numbers and social security numbers. Audience members are not to share any of their own information in these regards with Artists or other Audience members. Audience Interactions in bad faith, including but not limited to Stalking, Trolling, Doxxing, Spamming, Negativity, Ad-Hominem Attacks, Language Encouraging Hate or Violence, Destructive Criticism, Abusive Behavior, and Threats of Self-harm are not to be tolerated by any Artist. Such an Audience member acting in bad faith or encouraging actions of bad faith will be banned from the community and will be reported to authorities and/or government services when appropriate.

**Communiteering:** The act of positive audience moderation is the art of Communiteering. Chat moderation best practices have been well established on Twitch and other community platforms. Communiteering takes that moderation a step further to not only enforce community prohibitions, but to make it easy for the audience to behave towards the Art, the Artists, and towards each other’s in ways that are psychologically safe, emotionally enriching, that support the Six Values, Role Modeling, and the Artistic Movement that the Ensemble offers packaged with its works of Art.

**Levels of Support:** Audience members will have different levels of support and different needs. New audience members have a low level of support, and the objective of Artists is to welcome and include them, encouraging new audience members to become increasingly enthusiastic. Enthusiastic Audience members have had a longer time to get to know the Artists, the Ensemble, the Ensemble’s Products, and the objective of Artists is to Honor and Celebrate them. Artists agree to welcome and include new Audience members, and to honor and celebrate enthusiastic Audience members.

**Priority Suggestion Channels:** Artists in consensus may offer priority suggestion channels in online forums for improv shows. Suggestions made in those channels will be considered before public channels.

**Behind the Scenes:** Artists agree to use of Behind-the-Scenes video, audio and photography by any Artist engaged in creating Marketing Segments or Packaging under the condition that such segments do not violate the terms of the Agreements on Supporting Other Artist’s Art. No conversation or information held in trust and no personal private information, including incidental breaches of said information, is to be shared with Audiences.

**Thank You Videos:** Artists are expected to produce short thank you videos thanking enthusiastic supporters for larger contributions. These videos are to be produced within two weeks of receiving support, and will be posted on Artists’ Platforms and Broadcast in Framing Packaging. In the event an Artist cannot produce a video in that amount of time, the Artist will let the Ensemble know as soon as possible so other Artists can cover for them. An older video may be used, one cut together artistically, or one recorded by another Artist thanking on the absent Artist’s behalf.

**Audience Community Groups:** Artists who create online forums for audiences to gather and interact using technologies including, but not limited to Discord, Facebook, Slack, or Patreon are expected to use good Communiteering practices, as well as implement all Agreements on sharing and interacting with Audiences in the administration of their groups. It is further expected that all Artists will be invited to such platforms as well to help support and engage the audience.

**Interviews:** Artists are expected to participate in interviews, panel discussions, ask-me-anything events, and other similar Packaging as time allows. Committing to such an event is a serious commitment, and all Artists agree to hold to their word if they will be present for an interview. If they cannot be present due to events outside their control, the Artist will let the appropriate people know as soon as they themselves do to give as much time as possible to finding a cover for the absence.

**Exclusive Access:** Certain channels in an online forum, and certain parts of the Art creation area may be marked for exclusive access to enthusiastic supporters. It is expected for Artists to occasionally visit such areas, interact, and thank the supporters that are granted access there.

---
## **Exit Conditions**

**Disengagement:** Artists may disengage from the Ensemble at any time and for any reason. Disengagement means the Artist is not longer a member of the Ensemble. What contributions they made remain part of their contribution to the Art. Disengaging from the Ensemble is indicated by an Artist writing "disengaged" next to their name, and noting the time with initials. and they will safely leave the work area.

**Ejection:** An Artist who is Ejected from the Ensemble is in clear and flagrant violation of the Working Agreement. By their actions they assert that they are unwilling and/or unable to adhere to the Working Agreement defined herein. Such an Artist is therefore unwelcome to return in future Ensembles.

**Amending this Agreement:** The Ensemble may, by consensus, elect to amend this agreement. In such a circumstance, the amendment is to be appended to this Agreement. Any statements in the Agreement found in conflict with the amendment, and any statements in prior amendments found in conflict with the most recent amendment are to be disregarded in favor of the most recent amendment. The amendment will be signed and dated by all Artists and is effective upon the date of the latest dated signature on the amendment.

**Nullification of this Agreement:** The Ensemble may, by consensus, elect to nullify this agreement in favor of another agreement or in favor of none. A statement of nullification signed and dated by all Artists must accompany this document. Nullification is effective upon the date of the latest dated signature on the statement of nullification.

---
## **Signatures**

I hereby acknowledge, accept, and will adhere to, all of the rights, responsibilities, and conditions of the above Squirrelington Working Agreement as well as the Standard Terms and Conditions attached hereto as Exhibit A and incorporated herein as if set forth in full:


Signature_______________________________________________________________________________________________________Date__ /__ /____ <br>
Artist’s Printed Name ________________________________________________________________________________ <br>
General Boundaries __ or Boundaries List Attached __



---
## **Exhibit “A”**
## Standard Terms and Conditions

This Exhibit “A” Standard terms and Conditions is attached to and made an integral part of the Squirrelington Party-Agnostic Collaborative Agreement (hereinafter the “SPACA”) as if set forth in full therein. As such each signatory to the SPACA above agrees with and is bound by all the terms and provisions of this Exhibit “A” as is evidenced by their signature above containing the acknowledgement of this Exhibit “A”.

1. Assignment/Subcontracting
No Artist shall have the right to assign or subcontract any of its obligations or duties under this Agreement without the prior written consent of all other Artists to the Show, which consent shall not be unreasonably withheld or delayed.
2. Choice of Law or Governing Law
This Agreement shall be governed by and construed in accordance with the internal laws of the State of California, U.S.A., without reference to any conflicts of law provisions. Each Artist hereby submits to the exclusive jurisdiction of, and waives any venue or other objection against, any Federal court sitting in the State of California, U.S.A., or any California state court in any legal proceeding arising out of or relating to this contract. Each Artist agrees that all claims and matters may be heard and determined in any such court and each Artist waives any right to object to such filing on venue, forum non-convenient, or similar grounds.
3. Force Majeure
No Artist shall be held responsible for any delay or failure in performance of any part of this Agreement to the extent such delay or failure is caused by fire, flood, explosion, war, embargo, government requirement, civil or military authority, epidemic or pandemic, act of God, or other similar causes beyond its control and without the fault or negligence of the delayed Artist. The affected Artist will notify the other Artists in writing as soon as possible after the beginning of any such cause that would affect its performance.
4. Indemnity
Each Artist shall indemnify, defend, and hold the other Artists harmless from and against any and all claims, actions, suits, demands, assessments, or judgments asserted, and any and all losses, liabilities, damages, costs, and expenses (including, without limitation, attorneys fees, accounting fees, and investigation costs to the extent permitted by law) alleged or incurred arising out of or relating to any operations, acts, or omissions of the indemnifying Artist or any of its employees, agents, and invitees in the exercise of the indemnifying Artist’s  rights or the performance or observance of the indemnifying Artist’s obligations under this Agreement. Prompt notice must be given of any claim, and the Artist who is providing the indemnification will have control of any defense or settlement. Specifically, Artist hereby indemnifies the other Artists against any and all claims of Patent, Copyright, Trademark or Trade Secret provided by Artist and brought by any third party whatsoever.
5. Integration Provision or Entire Agreement
The SPACA and this Exhibit A thereto sets forth and constitutes the entire Agreement and understanding of all of the Artists with respect to the subject matter hereof. The SPACA and this Exhibit A thereto supersedes any and all prior Agreements, negotiations, correspondence, undertakings, promises, covenants, arrangements, communications, representations, and warranties, whether oral or written, of any Artist Party to this Agreement.
6. Limit of Liability
In no event shall any Artist be liable to any other Artist Party or any third Party in contract, tort or otherwise for incidental or consequential damages of any kind, including, without limitation, punitive or economic damages or lost profits or Intellectual Property infringement or violation, regardless of whether the Artist shall be advised, shall have other reason to know or in fact shall know of the possibility. In no event shall any Artist be liable for any incidental or consequential damages.
7. Notices
All notices shall be provided to Artists by their normal method of direct contact.
8. Severability
If any provision of this Agreement shall be declared by any court of competent jurisdiction to be illegal, void, or unenforceable, the other provisions shall not be affected but shall remain in full force and effect.
9. Written Modification
This Agreement may be amended or modified only by a writing executed by all Parties.
10. Counterparts
This Agreement will be executed in counterparts and Artist’s signature on at least one counterpart shall make the Agreement effective for the signing Artist.
11. Mediation and Arbitration  
**All disputes arising out of or related to this Agreement shall first be subject to Mediation between all Artists. In the event such dispute cannot be resolved through this Mediation process within Ten (10) days after a notice of dispute is given, then binding Arbitration shall be filed in Los Angeles, California pursuant to the rules of and utilizing the facilities and personnel of JAMS. In so agreeing, the Artists expressly waive their rights, if any, to a trial by a jury or decided by a judge of these claims and further agree that the award of the arbitrator shall be final and binding upon them as though rendered by a court of law and enforceable in any court having jurisdiction over the same.**
