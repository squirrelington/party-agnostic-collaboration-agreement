# Grounds for Ejection Incident Report

This report describes an incident that clearly and flagrantly disregards the SPACA or an associated written Working Agreement. The parties here aseert the incident, to their sound reasoning and judgement, is not an honest mistake on the part of the Collaborator. The parties further assert that the incident is in the scope and context of this SPACA, it's Art's creation, distribution, revenue administration, governance, or in the context of a written Working Agreement in effect during the creation of the Collaboration's Art. General conduct, public or private, is outside the scope of this report, and is not sufficient grounds for Ejection.

Incident Statement Made by <br>
Name:_____________________________________________________ <br>
Contact information:______________________________________

I swear to the compete truthfulness of the the statement below:

Signature_________________________________date:__ /__ /____ <br>

Witnesses:<br>
Name:_____________________________________________________ <br>
Contact information:______________________________________

Name:_____________________________________________________ <br>
Contact information:______________________________________

Name:_____________________________________________________ <br>
Contact information:______________________________________

Statement of Incident:
