# Audience Releases

These texts are to gain consent and allow in non-collaborating Audience members to be recorded (laugh, appaluad, and more) without not claiming copyrights or restrictitng the Art of the Collaboration or Ensemble.

### For Uncontrolled In-person Environments
where Audience may "walk-in" uninvited:
Post the text at points of entrance to the area where the recording is happening.

```
We're recording Art and you may be recorded!

By entering this area and by your presence here, you consent to be photographed, videographed and/or otherwise recorded. Your entry constitutes your consent to such recording and to any use, in any and all media throughout the universe in perpetuity, of your suggestions, contributions, appearance, voice and name for any purpose whatsoever in connection with the production presently entitled:

(SPACA Name): ________________________.

You understand that all Art creation will be done in reliance on this consent given by you by entering.
If you do not agree to the foregoing, please do not enter this area.
```

### For Controlled In-person Environments
where only those issued an invitation or ticket

Before purchasing a ticket, warn the ticket purchaser that they'll be asked to agree to the release.

```
We're recording and we need everyone's permission in case you get recorded too.
We'd love for you to come, though! Are you ready to get your tickets?
```

Include the text with the invitation or ticket confirmation:

```
We're recording Art and you may be recorded!

By your attendance, you consent to be photographed, videographed and/or otherwise recorded. Your attendance constitutes your consent to such recording and to any use, in any and all media throughout the universe in perpetuity, of your suggestions, contributions, appearance, voice, and name for any purpose whatsoever in connection with the production presently entitled:

(SPACA Name): ________________________.

You understand that all Art creation will be done in reliance on this consent given by you by attending.
If you do not agree to the foregoing, please do attend this event.
```

### For Online Environments
the release needs to be included in the application's Terms of Service, or by Notice before interacting with the service:

```
We're recording Art and you may be recorded!

By interacting with this work of Art, you consent to have those interactions recorded and incorporated into the work. Your interaction constitutes your consent to any use, in any and all media throughout the universe in perpetuity, of your suggestions, contributions, appearance, voice, and name for any purpose whatsoever in connection with the production presently entitled:

(SPACA Name): ________________________.

You understand that all Art creation will be done in reliance on this consent given by you by intecting with the Art.
If you do not agree to the foregoing, please do not interact.
```
