*Contributions are welcome!!*
**THANK YOU!!!**

Please open up an issue and start a discussion before submitting pull requests!

Issues will be used for discussion - flag concerns, or opportunities for improvement
 - on the philosophical intent of the Agreement
 - on the useability of the Agreement
 - and on various approaches for the Agreement to meet its intent and usability

 There will be a lot of discussion.  
 Constructive criticism is welcome.  
 Trolling and Harmful Negativity are not welcome.  

By participating in contributing, you agree to follow the applicable sections of the SPACA's Working Agreement.   
@IanDCarroll is the RSO, and the Repo's chief maintainer.  
Please be sensitive to different contributor's lived experiences and do your best to be encouraging where approriate.  
No intollerance, subtle or overt, will be tollerated.  

For the good of all contributors and the repo, If a contributor does not follow these guidelines, maintainers, at their sole discretion, may take any legally allowable actions necessary and possible to remove the contributor's influence.

Positive influence is very much welcome and appreciated!
Thank you for participating!!

**Branches:**  
 - `main` is not to be pushed to directly except for README and CONTRIBUTING commits.
 - `next-release` will be the development trunk for accepted changes waiting to be released as a new version.
 - PRs: fork, create a feature branch with your change, and submit PRs to `next-release`
 - Include a good readme description, say what your changing and why. Make sure to include the issue. We should be discussing changes in issues before submitting a PR.

**Versioning:**  
 - We'll be using semantic versioning using git tags. `A.B.C`
 - `C` version bumps are for non-functional wording formatting and grammar changes only they cahnge no function of the agreement.
 - `B` version bumps make a change in the function of the agreement so as to be more effective. These versions signers should begin to pay close attention to.
 - `A` version bumps are for large overhauls and drastic changes in how the agreement works to provide for its intent. These version bumps achieve the same ends but require the Collaborators to work and organize themselve in a different way to achieve that end.
 Older versions may continue to be used and developed from if prefered.


**Releases:**  
 - The last stage before a release is approved is to have an Entertainment Lawyer review the `next-release` branch. After they have made any necessary corrections, a release will be performed.
 - The cost of legal review for me is ~$2500.00. As such releases won't happen more frequently than once a year if I'm personally financing them. We can discuss alternate means of expediting releases if someone or all of us pool funds for it. Or a generous interested party may pay the sum of legal fee for review.