# Squirrelington Party-Agnostic Collaboration Agreement (“SPACA”)
## Version 2.1.1 - July 12, 2024

<div align="center">
   <img src="/uploads/8bd75818a3966c50d562690a5beff191/D1404__Squirrelington_Studios_Mascot_RGB.png" height="256">
</div>

A means of allowing the quick and grounded formation of Artistic Ensembles and Improv Troupes Streaming Art Online to share in Creation, Revenue, and Ownership of their recordings as freely and eqitably as possible.
*Think "stone soup", but an online show.*

**Be a Citizen of The Arts** and help shape our destiny!<br/>
Contribute to the discussion in [The Issues Forum](https://gitlab.com/squirrelington/party-agnostic-collaboration-agreement/-/issues)

| Drafters | |
| -- | -- |
| Ian Carroll | Jim Fiedler |

| Thought<br>Contributors | | |
| -- | -- | -- |
| Jessica Walker | Phinneas Kiyomura | Sarah Sunday |
| Sylvia Deal | Mae Milano | Steven Molony |

Note: the Version 2 Agreement has __not__ been reviewed by a lawyer yet, but you can help by contributing to our community's lawyer fees:<br/>
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/H2H0HE4SK)<br/>
make sure to mention "SPACA" in the message.<br/>
The text of the Agreemement was last reviewed and revised by an Entertainment Lawyer on `August 9, 2022`. <br/>
<br/>
[**Read the Agreement Here**](SPACA.md)

## **Contents**
 - [**Summary**](SPACA.md#summary)
 - [**Definitions**](SPACA.md#definitions)
    - [Contributions](SPACA.md#contributions)
 - [**Intellectual Property including Copyright Ownership**](SPACA.md#collective-intellectual-property-ownership-including-copyright-ownership)
 - [**Revenue Sharing**](SPACA.md#revenue-sharing)
    - [Revenue Administration](SPACA.md#revenue-administration)
 - [**The Collaborator’s Bill of Rights and Responsibilities**](SPACA.md#the-collaborators-bill-of-rights-and-responsibilities)
 - [**Governance**](SPACA.md#governance)
 - [**Exit Conditions**](SPACA.md#exit-conditions)
 - [**Legal Backbone (Exhibit A)**](#exhibit-a)

## **Useful Supporting Documents**
 - :movie_camera: [**For Interactive Audiences**](Supporting-Documents/Audience-Releases.md) => Convenient text for handling shows with audiences (what's the point, otherwise!)
 - :skull_crossbones: [**For Reserving your Special IP**](Supporting-Documents/List-of-Reseved-Intellectual-Property.md) => If there's something you've created you absolutely want no one using.
 - :trophy: [**Nominating Contributions**](Supporting-Documents/List-of-Contributions-Nominated.md) => Here's a form you can use to Nominate for Contributions.
 - :dizzy: [**Certfied Contribution List**](Supporting-Documents/List-of-Contributions-Certified.md) => Here's a form to record the Certified Contributions (and calculate payout percentages).
 - :heavy_check_mark: [**Record of Governance**](Supporting-Documents/Record-of-Governance.md) => Keep track of when and what descisions were made.
 - :moneybag: [**Record of Revenue**](Supporting-Documents/Record-of-Revenue.md) => Keep track of when you showed art, and what money you recieved.
 - :money_with_wings: [**Revenue Distribution Reciept**](Supporting-Documents/Revenue-Distribution-Receipt.md) => Simple example receipt with some nice details.
 - :angry: [**Statement of Passive Engagement**](Supporting-Documents/Statement-of-Passive-Engagement.md) => "Don't bother me about this agreement."
 - :sweat_smile: [**Statement of Active Engagement**](Supporting-Documents/Statement-of-Active-Engagement.md) => "JK, bother me now."
 - :bow: [**Stepping down from Distribution Coordinator**](Supporting-Documents/Statement-of-Stepping-Down.md) => When it's time to let someone else have responsibility.
 - :cry: [**Statement of Relinquishment of Rights and Revenue**](Supporting-Documents/Statement-of-Relinquishment.md) => If you need to leave for good.
 - :police_car: [**Grounds for Ejecton Incident Report**](Supporting-Documents/Grounds-for-Ejection-Incident-Report.md) => If there's serious trouble, the agreement has teeth.
 - :scroll: [**Example Working Agreement**](Supporting-Documents/Squirrelington-Working-Agreement.md) => This one this thorough, yours may be shorter or different.
 - :no_entry: [**Working Agreement Boundaries List**](Supporting-Documents/List-of-Boundaries.md) => A way to list more personal boundaries so you can work safely.
 - :nerd_face: [**Domain Model**](Relational-DB-Domain-Model) => A schema for constructing a Relational Database to store and manage multiple SPACAs for web and mobile applications.

---
## **What is this For?**

We’re going to work together as equals to create a story, then share the Revenue. To the extent that it is legally possible, we will also share the Intellectual Property rights. And we’ll learn from one another.

This is to empower and enable us as independent artists to create stuff without a big company or a powerful personality hogging the glory, and without our hands being tied to bar us from sharing and profiting from our individual labors.

Below describes the details of how this all works so that it’s fair to everyone.

What’s still needed, but is left to us to self-organize outside of this agreement, are specifics around:
 - A working agreement ([**Example**](Supporting-Documents/Squirrelington-Working-Agreement.md)),
 - studio gear and facilities,
 - technical and artistic expertise,
 - distribution, marketing, and merchandising strategies,
 - a Platform for gathering and focusing a joyful, engaged, non-toxic Audience
to bring our co-created Art to that Audience.

Participation in the story and the above activities is highly encouraged, as is support of each other’s artistic growth and success. It is also vigorously encouraged that Collaborators share their artistic and technical abilities, and to try out fields beyond their strong suits. All Collaborators are welcome to bring their own offerings so long as we coordinate our efforts to deliver the highest quality art we can without any unnecessary delays. We will also collaborate on how to improve the above efforts so that our offerings grow in goodness both for the Audience and for the team’s joy.

Theater Companies, production Companies, venues, or other business entities who share in this agreement will be considered Collaborators in their own right. The individual team members of a company are to be treated as Collaborators under the [The Collaborator’s Bill of Rights and Responsibilities](SPACA.md#the-collaborators-bill-of-rights-and-responsibilities), which guartantees certain optimal working conditions for all individuals. For all other matters, it remains a company’s purview to grant, manage, or reserve.

Source SPACAs may also be Collaborators on this SPACA, allowing for work to be shared between Collaborations, and for longer serialized works of Art to be created, such as franchises or ongoing streaming shows.

No outside copyright material! In order to preserve our independence, we can’t bring in any prop, music, sound effect, graphic, or representation that is the property or identifiably derivative property of someone else’s intellectual property, especially of major studios and record labels. Best bet is to make it yourself from your own imagination or ask another Collaborator to make it.

If Collaborators have conflicts with other Collaborators, we all agree to mediate internally before abandoning and reforming the Collaboration, before moving to arbitration, and before moving to court proceedings.

This agreement applies to a discrete work of art, such as, but not limited to a *single show*. This Art may be presented in parts or as a single piece that is deemed "Complete". Once it's Finalized, your rights and any Revenue share remain locked-in via this agreement for this recording only until agreed otherwise. If we do another show, we’ll sign another agreement very much like or identical to this one.

Interested? Read on!

[**Read the Agreement Here**](SPACA.md)
