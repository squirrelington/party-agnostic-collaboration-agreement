# Squirrelington Party-Agnostic Collaboration Agreement (“SPACA”)
## Version 2.1.1 - July 12, 2024

<div align="center">
   <img src="/uploads/5176a4df54a11939db5ffd19d72ea910/D1404__Squirrelington_Studios_Mascot_one_colour.png" height="256">
</div>

## **Contents**
 - [**Plain English**](#plain-english)
 - [**Summary**](#summary)
 - [**Definitions**](#definitions)
    - [Contributions](#contributions)
 - [**Intellectual Property including Copyright Ownership**](#collective-intellectual-property-ownership-including-copyright-ownership)
 - [**Revenue Sharing**](#revenue-sharing)
    - [Revenue Administration](#revenue-administration)
 - [**The Collaborator’s Bill of Rights and Responsibilities**](#the-collaborators-bill-of-rights-and-responsibilities)
 - [**Governance**](#governance)
 - [**Exit Conditions**](#exit-conditions)
 - [Signatures](#signatures)
 - [**Exhibit A**](#exhibit-a)

## **Summary**

This document is between multiple equal signatories called “Collaborators”, who together, will create a work of Art for presentation to Audiences, paying or otherwise, via distribution channels including, but not limited to online streaming, and internet video on demand. They will also create marketing and merchandising products that support and make use of the work of Art. It describes how the work of Art's Revenue and Intellectual Property such as copyright are to be shared in addition to other essential functions and guarantees.

#### **Drafters’ Indemnity:** 
This document is presented as-is and is not encouraged nor discouraged for use by the drafters of this document. It is agreed by all signatories of this document that any consequences to the signatories, or loopholes in the conditions are the fault of the writers of this document. Such writers are held blameless. All signatories agree to this document at their own risk and irrevocably waive any and all claims, of any kind or nature whatsoever, against any and all writers of this document.

---
## **Definitions**

**SPACA Name:** _______________________________________. All Art (defined below) and artifacts' file names will include this name, or be contained in containers that include this name. When referenced, this name shall identify this Agreement individually. If names between multiple agreements are identical, then distinguishing features, such as the last signature date, the location of production, or a description of the Art will be used to distinguish between the two. Any separately written Working Agreement will use this name as a reference.

**Collaboration:** The collective of all Collaborators (defined below) who are signatories of this agreement.

**Collaborator:** An Individual, Company, or Source SPACA (defined below), that brings a skilled Contribution to the Art (defined below). Collaborators will have expertise in at least one of the Contributions described below but are encouraged to practice more than one contribution and grow their skills in new areas to benefit their own independence, and to benefit the Art of this and future Collaborations. Collaborators are also expected to coach and mentor other Collaborators in their areas of expertise. Collaborators are credited with the Contributions they make (defined below).

**Individual:** An independent artist and/or technical expert, a single physical person, willing and able to contribute to the Art according to the Collaborator’s Bill of Rights and Responsibilities.

**Company:** A legal business entity such as, but not limited to a Theater Company, Performance Ensemble, Artist Collective, Technical User Group, Production Company, Studio, Theater, or Locational Venue. The individuals that comprise the business entity, such as, but not limited to its owners, employees, and contractors are to be treated in the Collaborator’s Bill of Rights and Responsibilities as if they were an Individual Collaborator. The Intellectual Property, Revenue Sharing, and Governance granted to the Company by this Agreement is the Company's own to share, manage, or reserve as it sees fit amongst its individuals.

**Source SPACA:** A Collaboration formed under an identical or similar copyright and revenue sharing agreement to this one, which has already been Finalized (see Governance Finalization below). A Source SPACA may only contribute its Completed Art toward this Agreement. A Source SPACA may have some or all of the same Collaborators that are in this agreement but is considered a discrete entity. It is bound to this SPACA as if it were another Collaborator separate from all other Collaborators signing in this Agreement even if some or all Collaborators are Signatories of both Agreements. The intent of this permission is to allow for the sharing of Art that honors consent of the creators, and allows for copyright and revenue sharing across Collaborations. The intended uses include, but are not limited to: enabling artistic franchises, serial art, collections, albums, anthologies, and facilitating a vibrant artistic ecosystem where creators have a means to join in discussion through contributing further to previous artistic creations. (See Governance Permission of Source SPACAs to sign as Collaborators for further rules).

**Host SPACA:** A Collaboration formed under an identical or similar copyright and revenue sharing agreement to this one, which accepts one or more Source SPACAs as Collaborators. This SPACA can be a Host SPACA to an unlimited number of Source SPACAs as or among its Collaborators.

**Revenue:** The gross sum of funds received to experience, receive, or show support for the Art or the Art's Intellectual Property.

#### **Contributions:** 

Contributions represent elements that if removed from the Art would noticeably change its nature, quality, or integrity. They are used as an essential part of Revenue division. There are specific kinds of artistic and technical work that count as Contributions. A list of all Contributions to be considered for credit are listed below:

| Type | Description |
| ---- | ----------- |
| Broadcasting | Operation, maintenance, and troubleshooting of streaming encoders, encoding software and network connectivity to deliver robust quality Art live to Audiences. Recording during live streams. Recording outside of a live stream is credited to *Recording*. |
| Coaching | Facilitation and guidance toward the perfection of an ensemble's practice and artistry. |
| Communiteering | Moderation, cultivation, and retention of passionate and non-toxic Audience members. |
| Design | Creation of graphics, photos, costumes, props, lighting, or set dressing, creation of Show sequences and the Show format, creation of graphic designs for merchandise and marketing materials. |
| Development | Providing custom made-to-purpose technical solutions and tools to aid other Collaborators, or providing hardware or software to connect with and engage the Audience in the Art. |
| Direction | Initial Art conception; facilitating research in preparation for creating the Art; directing practice toward the Art concept. |
| Editing | Live switching of camera feeds, post-editing, adjustments to recorded assets, effects, and post-processing. |
| Engineering | Maintenance, troubleshooting, and coordination of the safe setup, teardown, and application of all tooling and equipment. |
| General Contribution | Credited only in the circumstance that a Collaborator is not credited any other Contributions. |
| Generative AI | Credit for the allowance by a Collaborator for the use of their voice, image, intellectual property including copyright, or identifiable artistic style by deep neural network algorithms to the end of creating additional Contribution elements or revisions beyond audio-visual enhancement effects, audio mastery, and color correction. |
| Marketing | Planning, coordination, and execution of Audience outreach and calls to action. |
| Music | Composition or improvisation of pre-recorded or live music. |
| Operation | Artistic or Cinematographic use of cameras, lights, sound, effects equipment, or specially-designed equipment created by *Development* during a live stream. Operation outside of a live stream is credited to *Recording*. |
| Organization | Scheduling and coordinating Shows and Collaborators in Shows. |
| Performance | Allowing yourself, for the purpose of contributing to the Art, to be perceived by the Audience or by active recording equipment whose recordings are presented to the Audience. This definition is left intentionally broad, and includes the requirement of the Collaborator’s express intent to perform so that artistic experimentation remains unlimited. |
| Recording | Photography, Cinematography, Sound Recording for post-processing of recordings to be assets in Art. Recording of Art for live streams is credited to *Broadcasting*. Photography, Cinematography, and Sound recording during a live stream is credited to *Operation*. |
| Venue | Preparing a recording/presentation space to meet the needs of Art creation, such as electrical, internet, acoustics, and rigging. |
| Writing | Words or stage directions that other Collaborators say or carry out. |

**Platform:** The sum of all distribution, marketing, and revenue-generating channels, be they physical or digital, including but not limited to store-fronts, websites, apps, social media channels and profiles that are owned, controlled, operated, or managed by a given Collaborator.

**Audience:** The collection of all persons who engage with the Art.

**Art:** The product made by the Collaboration. A discrete work of Art such as, but not limited to a single online streaming show presented live or as a post-produced video. The Art may be in any medium or format the Collaboration sees fit to create. The Art may be in several parts, or as one continuous work, so long as it is Complete (see Governance Complete below).

**Merchandise:** Any material goods offered for sale to the Audience using the Intellectual Property of the Collaboration.

**Marketing Materials:** Any materials, tangible or digital, offered at no cost to the audience for the purpose of Promoting or Packaging the Art or its Merchandise.

---
## **Collective Intellectual Property Ownership including Copyright Ownership**

### **Ownership as Equal as Possible:**

**Intellectual Property including Copyright:** Any currently-signed Collaborator may file for Intellectual Property protection, including but not limited to Copyright, for any assets created as a matter of course in producing Art, or for the Art itself, on the condition that the filing Collaborator include all signed Collaborators of this Agreement at the time of filing. A filing Collaborator must inform the currently signed Collaborators by normal agreed upon means of communication of their intent to file to prevent duplicate filings. Collaborators have the right to refuse to be included in the filing, provided they express so to the filing Collaborator within two days of being informed of the intent to file. Such Collaborators will share the cost of the filing divided evenly between all. A Collaborator may elect to provide a greater amount beyond their share of the cost of the filing to cover all, or part of a fellow Collaborator’s filing expense. Such elected provisions do not grant any additional rights to intellectual property, nor are the rights to intellectual property of the benefiting Collaborator lessened by the gift of the provision for filing costs.

**Needed Materials:** A Collaborator's Intellectual Property including copyright, voice, image, and contributions to the Art.

**Authorized Release:** A release to any use, in any and all media throughout the universe in perpetuity, for any purpose whatsoever in connection with the subject of the releasee.

**Unauthorized Use:** Any Collaborator using unauthorized copyrighted material from any source holds only themself to blame and holds blameless all fellow Collaborators from fault. A Quality Stop (see below) will be ordered to fix any unauthorized copyrighted material in the Art.

**Use of Copyright Material:**
   **Without Permission:** All Collaborators agree not to use unauthorized copyrighted material outside of free use.
   **Conditions of Allowable Permission:** Any agreement releasing copyright for material outside the Collaboration’s ownership must authorize every member of the Collaboration to nothing less restrictive than an Authorized Release of the material to the Art.
   **Collaborator Copyrights and Intellectual Property:** Copyright or Intellectual Property owned by a Collaborator is reserved by the Collaborator. All Collaborators herein grant an Authorized Release of their Needed Materials to the Art. Any Collaborator's Intellectual Property, voice, or image may be excluded from this release in a list of reservations attached to this agreement.

**Generative Artificial Intelligence Permissions:** Generative Artificial Intelligence (Hereafter called AI) may be used for quality enhancements such as, but not limited to, audio-visual effects, audio mastery, and color correction. Use of AI for narrative additions and revisions, such as, but not limited to, creating additional Performance Contribution elements, or the insertion of product placement or endorsements requires the approval of each Collaborator being emulated or affected by AI. AI may not be used to directly emulate actual persons without an Authorized Release of the specific Needed Materials being emulated in the Art for the Art. For Art scheduled for presentation within 48 hours, the tool used to make modifications must be reviewed and approved by each Collaborator being emulated. For all other Art, the generated elements of the art must be reviewed and approved by the emulated Collaborators. A Collaborator may issue a Quality, Safety, or Context Stop if the results of AI detracts from the quality, causes safety concerns, or sends an undesired message with the art being expressed.

**Grant of Use:** All Collaborators are hereby granted, in perpetuity, the worldwide, royalty-free use of the Intellectual Property, including Copyright, of the Art produced by the Collaboration including all rights declared below.

**Equal Storage Rights:** Each Collaborator reserves the right to archive and preserve the Art independently.

**Equal Distribution Rights:** Once the Art is Complete (see Governance Complete), Each Collaborator reserves the right to distribute the Art on their Platform as they see fit. A Collaborator distributing the Art agrees to give undelayed notice to the rest of the Collaboration via normal agreed upon means of communication when and where the Art is available on their Platform.

**Equal Pricing Rights:** Each Collaborator reserves the right to charge subscription fees, and sell tickets for viewing of the Art. Each Collaborator reserves the right to collect tips, donations, and monetary support from the Audience before, during, and after the Art has been broadcast. Each Collaborator reserves the right to set their own prices for Art as the given Collaborator sees fit. Each Collaborator reserves the right to offer Art without charge to the Audience at the given Collaborator’s sole discretion.

**Equal Merchandising Rights:** Each Collaborator reserves the right to create merchandise using the Art's Intellectual Property or hire professionals to create branded merchandise based on the Art's Intellectual Property. Each Collaborator reserves the right to sell merchandise for profit, or to give such merchandise away without charge.

**Equal Promotion and Marketing Rights:** Each Collaborator reserves the right to use elements of the Art to promote the Art, promote their fellow Collaborators, and promote themself on their Platform. Each Collaborator reserves the right to hire outside advertisers, marketers, advertising agencies, and advertising Platforms to create and distribute marketing and promotional materials on the internet, or by any means deemed effective.

**Disagreements in Strategy:** All Collaborators agree that all other Collaborators have an independent right to offer, price, strategize, and engage Audiences with the Art on their own Platforms as they see fit. Collaborators agree to consider the offerings of other Collaborators when formulating their own offerings. Where such offerings are perceived as destructive or undermining to one’s own marketing strategy, Collaborators agree to respectfully and informally discuss ways in which they can better support each other in optimizing the generation of the Collaboration’s Revenue as described in the section on Revenue Sharing before moving to mediation, or binding arbitration described in Exhibit A.

**Quality Stop:**  Within two weeks of first notice of distributing the Collaboration's Art, each Collaborator has the right to exercise a Quality Stop on the Art presented by their Collaborators on any means of distribution to an audience. When such a Quality Stop is issued via video-on-demand, the Art must be taken down as soon as is reasonably possible. When such a Quality Stop is issued on live performances, such Art must cease after the current broadcast. The Quality Stop is to be exercised only to address quality issues that were not caught earlier.

The Collaborator who issued the Quality Stop will work with the Collaborator who distributed the Art to find an editing or post-production fix and return the improved Art to the Audience as quickly as is reasonably possible, and within one week of the Quality Stop being issued. In the event such a fix is impossible, or two weeks elapse in the course of finding a fix, the Art will be pulled from the Collaboration’s platforms permanently. All Collaborators agree to meet for a blameless post-mortem to determine how the issue was not remedied before it was distributed. From the insights generated from that post-mortem, a process plan will be created for how to prevent the same issue from repeating.

**Safety Stop:** Each Collaborator has the right to exercise a Safety Stop on the Art presented by other Collaborators on any means of distribution to any Audience. When exercised, the presenting Collaborator agrees to take down the Art as soon as is reasonably possible on any or all means of distribution to any or all Audiences the Collaborator exercising the Safety Stop requests until that exercising Collaborator deems it safe to present again. Safety Stops are to be used only in instances where displaying the Art by certain means of distribution to certain Audiences have a likelihood of harming the the exercising Collaborator by state incrimination, loss of employment, assault, battery, abuse, or harassment by organizations or persons known or unknown to the Collaborator.

**Context Stop:** In the special circumstance where a Collaborator perceives that world events converge so that viewing the Art now delivers an unintended, unfavorable, or destructive message, that Collaborator can call for a Context Stop. The Collaborators will meet to discuss whether to fix the Product, or pull down all of the Product offerings in question from the Collaboration’s Platforms and archive the Art indefinitely, or until such time as world circumstances no longer weigh on the minds of Art appreciators to distort the Art’s intended meaning. Such a decision will be made by a Two Thirds Vote (see Governance Two Thirds Vote).

**Disallowance of Petty Stops:** Under no circumstances, besides the Quality Stop, Safety Stop, or the Context Stop, may one Collaborator request another Collaborator cease offering Art on that other Collaborator’s Platform. All other Stop Requests are defined herein as a Petty Stop and disallowed.

**Subcontracting to Greatly-Larger Outside Companies:** A Large Outside Company is defined here as an outside party which has reported a gross annual revenue greater than ten times a potentially contracting Collaborator’s gross annual revenue in the last fiscal year, and has reported a gross annual revenue greater than twelve million dollars in the last fiscal year. Any Collaborator seeking to Subcontract with or Assign to a Large Outside Company will first seek agreement granted by a Two-Thirds Vote in the affirmative (see Governance, Two Thirds Vote). In such a case, all Collaborators owning Intellectual Property agree to meet to deliberate for no less than one hour before voting to subcontract, assign, or no. Such a meeting shall not be unreasonably delayed for more than two weeks. Assignments granted in a Collaborator’s last will and testament are exempt from the above.

**Estates:** In the instance that a Collaborator in this agreement dies, their Intellectual Property rights and ownership will go to their lawful heirs, successors, assigns or estate.

---
## **Revenue Sharing:**

**Responsibility for Cost of Doing Business:** All Collaborators come at their own risk, contributing what resources they will. Each Collaborator will incur on themself the cost of their own operations. Such costs may include but are not limited to rent of premises, the rent of cloud infrastructure servers, software subscriptions, service fees, insurance, taxes and fines, studio equipment, fixed upfront capital expenses, costume, prop, or set design expenses, delivery fees, traveling expenses, graphic design or artistic service commissions, third party post-production, raw materials, and manufacturing costs. Each Collaborator will not hold its Collaborators or the Collaboration accountable for any costs or monetary losses incurred in the act of creating, storing, or distributing the Art.

**All Revenue must be Shared:** Any Collaborator receiving Revenue must divide and distribute that Revenue amongst all Collaborators, including themself by the method of division defined below. Any Revenue collected before the Art is Finalized will be held until Finalization.

**Revenue from Tangible Merchandise:** Revenue generated from tangible merchandise shall be counted and distributed after the cost of manufacturing and materials of the unit sold have been met. Funds received for Tax and Shipping will not be counted as Revenue. Merchandise that contains no reference to the Collaboration's Art or the Art's Intellectual Property are exempt from Revenue Sharing. Records of costs versus Revenue must be kept and open to Collaborators for audit when subtracting costs.

**Dividing Revenue amongst Collaborators:** 
All Collaborators are considered equal. All Contributions are considered equal in value to the Art. Qualitative differences between Contributions of the same type, or of different types, are considered unmeasurable.

   **50% by Collaborator:** The first 50% of the Revenue is divided per Collaborator and credited to each equally.
   **50% by Contribution:** The rest is divided per Contribution, then credited to each Collaborator according to their Contributions.
These credits are summed per Collaborator.
 
If one collaborator's sum is greater than 50% of the revenue, see the rules on 50% Cap for Sole Majority Prevention. After any modifications required by a 50% Cap, the funds are to be distributed to each Collaborator, according to Revenue Administration, defined below.

  **50% Cap for Sole Majority Prevention:** If after Dividing Revenue, one Collaborator is allocated a percentage greater than 50% of the distribution, that Collaborator instead gets 50% of the distribution. The remaining Revenue is divided between the other Collaborators based on their Contributions, one share per Contribution. This cap is to prevent the emergence of a de facto artistic authority, and to encourage an excellent contributor to mentor, share responsibility, and give other Collaborators a chance.

  **Fractional Cents Reconciliation:** Revenue distributions with fractional cents are to be rounded down to the nearest cent.

### **Revenue Administration:**

**De Facto Revenue Administration Election:** By the act of collecting funds at any time, at any place, or on any software platform in exchange for access to, or in support of the Collaboration, or of the Collaboration’s Art, such a Collaborator declares themself to be a Revenue Administrator and agrees to all conditions herein.

**Open Financial Records:** Revenue Administrators agree to keep open books to the rest of the Collaborators maintained in a state at least complete enough for tax reporting purposes.

**Receipts:** All payouts will include a receipt that reports the total amount distributed to the Collaborator in the period of record.

**Means of Payment:** A Revenue Administrator may determine the payment method for their payments to Collaborators. Payments will be held until a Collaborator has provided a means of fulfilling payment by that payment method. Revenue Administrators will make every reasonable effort to establish a means of payment with Collaborators. Payments held for more than three years will be forfeited by the Collaborator and redistributed evenly amongst all other Collaborators.

**No more than Three Month’s Delay:** Revenue Administrators will calculate, divide, and deliver payments to all Collaborators with a means of payment in time not to exceed three months from the date of receiving Revenue. This time scale is for the purposes of guarding against chargeback orders, and to allow for reasonably batching Revenue distributions.

**Agreement to Mediation:** In the event of a discrepancy or dispute over those records or payments that can't be personally resolved, it is agreed that mediation should be made between all affected Collaborators before any further action is taken.

#### **Online Subscriptions and Collections:**

**Collection:** a given Collaborator's Revenue-generating Platform channel or account offering the Art among its videos on demand.

**View:** Outside of a marketing context, a record of at least one Audience member experiencing any meaningful portion of the Art by any means. On Collections where metrics are uncontrolled by the Collaborator, a View is whatever the Collection counts as a "view". Collaborators agree to only sum actual Audience views minus their own views for videos in their Collections to the best of their ability.

**Total Views:** For a given Collaborator's Platform channel or account, the sum of Views of all offered videos during the subscription period or period of record. 

**View Share:** The revenue generated on a Collection divided by the Total Views during the same period of record.

**Video on Demand Collection Subscriptions:** Revenue is the View Share times the Art's Views during the period of record.

**Free-to-View Video on Demand Collections:** Revenue that supports this Art expressly will be distributed according to Revenue Sharing. Funds that can't be attributed to this Art or to other offerings in the Collection will be divided the same as a Video on Demand Collection Subscription. Revenue Administrators agree to take reasonable steps to minimize unattributed funds received.

**Estates:** In the instance that a Collaborator in this agreement dies, their further distributions of Revenue will go to their lawful heirs, successors, assigns or estate.

---

## **The Collaborator’s Bill of Rights and Responsibilities:** 

It is agreed that all Collaborators will work together through a Working Agreement. That Working Agreement may be written, spoken, implied, or assumed. The Working Agreement is left vague within the SPACA to support innovation, but shall support the following principles:

   1. **Each Collaborator has agency.**
      1. Know, share, and enforce your boundaries; respect the boundaries of others.
      2. Respect privacy and the right to not be labeled.
   2. **Be safe and ensure the safety of others.**
      1. Practice physical safety around equipment, performing stage combat, and intimate acts.
      2. Practice psychological safety, including but not limited to plans for addressing microaggressions and trauma triggers.
   3. **Be welcoming and warm to each other and to the audience.**
      1. Welcome all gender expressions including gender-nonconformity.
      2. Welcome diversity of thought, of background, and of lived experience.
      3. Do not tolerate intolerance.
   4. **Value emergent discoveries over a singular artistic vision.**
      1. Allow every Collaborator’s skill to be a first-class contribution to the Art.
      2. Grant that every Collaborator has an equal right to contribute to the Art.
      3. Let go of the need to control; let Art emerge from collaboration.
   5. **Seek out improvements, and innovations both individually and as an organization.**
      1. Question assumptions; look for better, faster, higher-quality ways to create our Art.
      2. Look for opportunities to enhance flow and joy in our work together.
      3. Be open to change and find opportunities to be changed.
   6. **Support each other’s artistic growth.**
      1. Commit to push comfort zones, but not boundaries.
      2. Allow time to practice continual learning.
      3. Share what you know.
      4. Give others a chance.

---
## **Governance**

Below are the conditions and definitions of decision-making actions without a central authority.

**Vote:** The means of collective decision-making for the governance of the SPACA and its Art. A Vote may be called for in any of the below circumstances. A Vote is said to be cast when received by the Collaboration. A Vote may remain uncast, or be cast in the negative or affirmative. Collaborators agree to abide by the decision of a Vote and agree not to call for repeat Votes without substantive changing conditions to warrant a second call.

**Majority Vote:** An action taken by the Collaboration, which is binding and active once a simple majority, greater than 50%, of all Collaborators vote in the affirmative.

**Second Approval:** An action taken by a Collaborator on behalf of the SPACA that requires a second Collaborator to review and co-sign for the decision to be binding and active. A Second Approval also requires all other Collaborators to be informed with all haste via electronic communication within one hour.

**Overriding a Second Approval:** Within 48 hours of notification, and While legally allowable and possible for a decision to be nullified, a Second Approval can be overridden and made null by a Majority Vote. In the circumstance of a Vote, positive or negative, that vote is reverted to uncast. In the circumstance of Signing any legal agreement or authorization, that signature is null and void.

**Two-Thirds Vote:** An action taken by the Collaboration, which is binding and active once a two-thirds majority, greater than or equal to 66%, of all Collaborators vote in the affirmative.

**Consensus:** An action taken by the Collaboration, which is binding and active once each and every Collaborator votes in the affirmative.

**Minority Rights:** A Collaborator who is out-voted or overridden has the right to post their reasons why they believe the vote is a wrong decision. Such reasoning is allowable to be publicly posted online with the sanction of the Collaboration. Such statements are the opinion of the Collaborator who made them and such a Collaborator releases collective responsibility for their remarks by any other Collaborator, or by the Collaboration as a whole. All Collaborators will exercise decorum, hold private conversations or information shared in trust by other members of the Collaboration private, hold Personally Identifiable Information private, and will refrain from any hate speech or ad-hominem attacks in their statements or risk Ejection from the Collaboration (see Exit Conditions below).

**Complete:** Art is "Complete" if:
   1. It’s “Live”: Has a date of presentation less than 48 hours after the last Collaborator signs the SPACA.
   2. A Majority Vote affirms it is complete via a Vote of Completion.
   3. There are no votes in the negative, and it has been 14 days since a call for a Vote of Completion.

If there is no Vote of Completion in progress, a new Vote of Completion can be called for by any Collaborator. If none of the above conditions can be met, the Vote of Completion is said to have failed. A new Vote of Completion may be called after changes to the Art.

**Contribution List Certification:** Collaborators agree to nominate each other for all and only Contributions that meet its definition (see Definitions Contributions).

   1. **Call for Nominations:** After the Art is Complete, a call for nominating Contributions will be asked of each Collaborator. A Collaborator nominates by listing which Contributions they know each Collaborator, including themself, Contributed. They may also list which Contributions they know a Collaborator, including themself, did not contribute as a negative nomination. A Collaborator will not be credited more than once for any given type of Contribution, but multiple Collaborators may be credited with the same Contribution type.

   2. **Resolving the Nominations:** One negative nomination discounts one affirmative nomination. Any Contribution with two nominations to the same Collaborator is certified as credited. Any Collaborator left with no Contributions is credited once with a General Contribution credit.

   3. **Certifying the List:** A Certified list of Collaborators and their Contributions will accompany the SPACA. Contributions are considered certified if all nominations received have been resolved to a certified list of Contributions and either:
      1. 14 days time has elapsed after the Art is Complete.
      2. A nomination has been received from every Collaborator.

All Collaborators agree that the Certified Contribution List represents all Collaborator Contributions for the purposes of revenue distribution

**Finalization:** The SPACA is Finalized if there are two or more Collaborators signed, the Art is Complete, and an accompanying Contribution List has been Certified. A Finalized SPACA can accept no new Contributions or Collaborators. Any further modification to the Art will be conducted only by Quality or Context Stops, or by inciting a new Host SPACA with this SPACA authorized as a Source SPACA Collaborator.

**Approval of Donation Links:** Calls for the Audience to Donate to causes or persons can be approved by a Majority Vote.

**Hosting of Source SPACAs as Collaborators:** This Agreement can be a Host SPACA; Source SPACAs are permitted to sign as Collaborators. To allow time for a possible Overriding Vote by the Source SPACA's Governance, **Source SPACAs must sign more than 3 days in advance** of any scheduled presentation, or Vote of Completion.

**Revenue Distributions to Source SPACAs** The Host SPACA's Revenue Administrators will pay distributions due to the Source SPACA to a single authorized designated payee. In the event no payee is designated, funds will be held until such a payee is designated and authorized by the Source SPACA.

**Authorization as a Source SPACA Collaborator in a Host SPACA:** This SPACA may be authorized as a Source SPACA if: 
   1. The Host SPACA will not be Completed more than 3 days from the time of signing (to allow for a possible Overriding Vote),
   2. A Collaborator signs a Host SPACA authorizing this Agreement as a Source SPACA,
   3. A separate Second Collaborator signs Second Approval,
   4. The Second Approval has not been Voted Overridden.

If all four of the conditions are met, then each Collaborator grants an Authorized Release of the Art to that Host SPACA. On all questions requiring a vote on the Host SPACA's Governance, Any Collaborator with a Second Approval from this SPACA can make the vote on its behalf. The first Signing Collaborator will be the Revenue Administrator for monetary distributions to the Collaborators of this Source SPACA from the signed Host SPACA. If a Collaborator becomes unable or unwilling to be a Revenue Administrator, the first Collaborator to volunteer is the new Revenue Administrator.

**Remastering a Live Performance:** A live performance may be edited/re-mastered afterward from recordings if:
   1. A Stop is called (see Collective Intellectual Property Ownership Quality Stop and Context Stop)
   2. A Collaborator incites a new Host SPACA and authorizes this SPACA as a Source SPACA. Additional Contributions needed to edit or re-master the original live performance are credited to the Collaborators who contribute them.

**Creating a SPACA Franchise:** A franchise, series, or show format can be created with a Source SPACA that individual Host SPACAs will use as expressions of the franchise.

**Creating a SPACA Collection:** A collection, album, or anthology can be created with a Host SPACA where each item in the collection is a Source SPACA.

**Passive Engagement:** A Collaborator may elect to be Passively Engaged with this Agreement. A Passively Engaged Collaborator cannot call for any Votes. Except for Votes to Amend, Nullify, or Eject, a Passively Engaged Collaborator will not be called to Vote after the time of their Passive Engagement and will not be counted as uncast in those calls to Vote. They also relinquish their right to call for Quality and Context Stops. If a Revenue Administrator wishes to Passively Engage, they must cease collecting Revenue and settle all unresolved distributions first. A Distribution Coordinator must step down from their role to become Passively Engaged. They must still hold their books open to Collaborators and obey all tax reporting Laws. A Collaborator is passively engaged by a dated statement of Passive Engagement attached to this Agreement.

**Active Engagement:** After no less than 24 hours of electing to be Passively Engaged, a Passively Engaged Collaborator may elect to become Actively Engaged again. When Actively Engaged, the conditions of Passive Engagement are nullified. An Active Collaborator may Vote in any call to vote dated before they elected to be Passive and after they elect to be Active again. A Collaborator is Actively engaged by a dated statement of Active Engagement attached to this Agreement. All Collaborators that are not Passively Engaged are Actively Engaged by default.

**Distribution Coordinator:** When a Distribution Coordinator is elected, all Collaborators but the Distribution Coordinator suspend their specific rights to Free Use, Distribution, Pricing, Merchandising, Marketing, Revenue Administration, Authorizing and Voting on Host SPACAs and no others. During their term, it is the Distribution Coordinator's responsibility to set strategy for these functions. The Distribution Coordinator may assign responsibilities with the consent of the assignee for each of these functions. Their term is indefinite, and they may choose to step down at any time. If no Revenue has been generated over the course of one calendar year, the Distribution Coordinator's role ends. All specific rights suspended are reinstated. All Collaborators agree to treat any Distribution Coordinator candidate, current, or past, as equal in every way to every other Collaborator. Any Collaborator in this role agrees to graciously yield their responsibilities upon their term's end.

**Vote of Distribution Coordination:** Any Collaborator can call for a Vote of Distribution Coordination. If there is a current Distribution Coordinator, a call for a new Vote cannot occur until one year after the election. Any Collaborator can nominate another Collaborator with their consent to be on the ballot, or "No Distribution Coordinator". A Majority Vote in the positive for any Candidate, or none, elects that option, otherwise the current condition remains in effect.

**Vote of Ejection:** Any Collaborator with Grounds can call for a Vote for Ejection (see Exit Conditions) on any potentially violating Collaborator once per incident. A Two-Thirds Vote in the affirmative within 14 days of the call for a Vote Ejects the accused Collaborator at the time of the last affirmative Vote. In the event that the Working Agreement elects Safety Officers, those Safety Officers while on duty are authorized to Eject a Collaborator by their own consensus at the time of their consensus in the interest of the Collaboration's safety. If the call for Ejection is an elected Safety Officer, a Two-Thirds Vote of all collaborators Ejects the Officer. An incident report must accompany this Agreement stating the action that was grounds for Ejection, its date and time, and witnesses to the action.

**Vote of Upgrade:** Any Collaborator may call for The Collaboration's Majority Vote to Upgrade this Agreement to the text of a SPACA tagged on the official project repository with a semantic version number greater than the current. The vote will conclude after 14 days from the call. A call for a Vote to Upgrade may be made once per version number.

**Vote of Amendment:** The Collaboration may, by Consensus, vote to amend this agreement. A collaborator with a Second Approval may call for a Vote to Amend. The call to vote will include the proposed amendment. Such a vote will conclude after 14 days from the call. If the vote is in the affirmative, the amendment is to be attached to this Agreement. Any statements in the Agreement found in conflict with the amendment, and any statements in prior amendments found in conflict with the most recent amendment are to be disregarded in favor of the most recent amendment. The amendment will be signed and dated by all Collaborators and is effective upon the date of the latest dated signature on the amendment.

**Vote of Nullification:** The Collaboration may, by Consensus, vote to nullify this agreement. A Collaborator with a Second Approval may call for a Vote to Nullify. Such a vote will conclude after 14 days from the call. If the vote is in the affirmative, a statement of nullification signed and dated by all Collaborators must accompany this document. Nullification is effective upon the date of the latest dated signature on the statement of nullification.

---
## **Exit Conditions**

**Relinquishment of Rights and Revenue:** A Collaborator may choose to exit this agreement by relinquishing their rights to the Art and their share of any further Revenue. If a Collaborator desires to exit in this way they must provide a signed and dated statement relinquishing their Rights and Revenue. That statement is to be kept alongside the agreement. The Relinquishing Collaborator steps down if they are a Distribution Coordinator, and will not engage further in Governance or be counted in calls to Vote. Their permissions to use their voice and likeness, and their grant of use to their Intellectual Property including copyright already contributed to the Collaboration's Art remain in effect, but, to the extent allowable by law, they release their claims to Intellectual Property including copyright to the Collaboration's Art. All Contributions credited to the Collaborator are to be discounted in all future Revenue Divisions. Final payouts for any Revenue shares earned prior to the Relinquishment will be paid at the earliest reasonable opportunity by all Revenue Administrators. If the Relinquishing Collaborator is a Revenue Administrator, the Collaborator will cease collecting Revenue and settle all distributions at the earliest reasonable opportunity.

**Grounds for Ejection:** Grounds is defined as an incident that clearly and flagrantly disregards the SPACA or an associated written Working Agreement. It is not an honest mistake. General conduct, public or private, outside the scope of this agreement or an associated written Working Agreement, is not sufficient grounds for Ejection. The Collaboration must provide a report of evidence including records and statements by witnesses warranting Grounds for Ejection. That report is to be attached to the SPACA upon Ejection.

**Ejection:** A Collaborator is Ejected from the Collaboration if they are so Voted with Grounds for Ejection. By signing below, a Collaborator agrees to all of the following if they are Ejected: The Collaborator...
  1. Asserts by their actions, they are unwilling and/or unable to adhere to the terms of this Agreement.
  2. Grants an Authorized Release of their Needed Materials to the Art.
  3. Releases any and all claims granted to them by this Agreement including but not limited to ownership and revenue.
  4. Ceases collecting Revenue for the Art and removes the Art from their Platform.
  5. Will Distribute any collected undistributed Revenue according to Revenue Administration rules.
Final payouts for any Revenue shares earned prior to the Ejection will be paid to the Collaborator at the earliest reasonable opportunity by all Revenue Administrators. Any further calculation of revenue will be divided without the Ejected Collaborator or their Contributions.

---
## **Signatures**

I hereby acknowledge, accept, and will adhere to, all of the conditions of the above Squirrelington Party-Agnostic Collaboration Agreement (“SPACA”), Its Plain English, Definitions,  Collective Copyright Ownership, Revenue Sharing, Rights and Responsibilities, Governance, and Exit Conditions, as well as the Standard Terms and Conditions attached hereto as Exhibit A and incorporated herein as if set forth in full:
<br>
<br>

Signature________________________________________________________Printed Name___________________________________Date__ /__ /____ <br>
Contact Email: _________________________________________________________________________________________________________________ <br>
If Company or SPACA: <br>
Source SPACA Second Approval_____________________________________Printed Name___________________________________Date__ /__ /____ <br>
Legal Entity DBA / Source SPACA name____________________________________________________________________________________________ <br>

<br>
<br>

Signature________________________________________________________Printed Name___________________________________Date__ /__ /____ <br>
Contact Email: _________________________________________________________________________________________________________________ <br>
If Company or SPACA: <br>
Source SPACA Second Approval_____________________________________Printed Name___________________________________Date__ /__ /____ <br>
Legal Entity DBA / Source SPACA name____________________________________________________________________________________________ <br>

<br>
<br>

(Add More signatures here as needed)

---
## **Exhibit “A”**
## Standard Terms and Conditions

This Exhibit “A” Standard terms and Conditions is attached to and made an integral part of the Squirrelington Party-Agnostic Collaborative Agreement (hereinafter the “SPACA”) as if set forth in full therein. As such each signatory to the SPACA above agrees with and is bound by all the terms and provisions of this Exhibit “A” as is evidenced by their signature above containing the acknowledgement of this Exhibit “A”.

1. Assignment/Subcontracting
No Collaborator shall have the right to assign or subcontract any of its obligations or duties under this Agreement without the prior written consent of all other Collaborators to the Show, which consent shall not be unreasonably withheld or delayed.
2. Choice of Law or Governing Law
This Agreement shall be governed by and construed in accordance with the internal laws of the State of California, U.S.A., without reference to any conflicts of law provisions. Each Collaborator hereby submits to the exclusive jurisdiction of, and waives any venue or other objection against, any Federal court sitting in the State of California, U.S.A., or any California state court in any legal proceeding arising out of or relating to this contract. Each Collaborator agrees that all claims and matters may be heard and determined in any such court and each Collaborator waives any right to object to such filing on venue, forum non-convenient, or similar grounds.
3. Force Majeure
No Collaborator shall be held responsible for any delay or failure in performance of any part of this Agreement to the extent such delay or failure is caused by fire, flood, explosion, war, embargo, government requirement, civil or military authority, epidemic or pandemic, act of God, or other similar causes beyond its control and without the fault or negligence of the delayed Collaborator. The affected Collaborator will notify the other Collaborators in writing as soon as possible after the beginning of any such cause that would affect its performance.
4. Indemnity
Each Collaborator shall indemnify, defend, and hold the other Collaborators harmless from and against any and all claims, actions, suits, demands, assessments, or judgments asserted, and any and all losses, liabilities, damages, costs, and expenses (including, without limitation, attorneys fees, accounting fees, and investigation costs to the extent permitted by law) alleged or incurred arising out of or relating to any operations, acts, or omissions of the indemnifying Collaborator or any of its employees, agents, and invitees in the exercise of the indemnifying Collaborator’s  rights or the performance or observance of the indemnifying Collaborator’s obligations under this Agreement. Prompt notice must be given of any claim, and the Collaborator who is providing the indemnification will have control of any defense or settlement. Specifically, Collaborator hereby indemnifies the other Collaborators against any and all claims of Patent, Copyright, Trademark or Trade Secret provided by Collaborator and brought by any third party whatsoever.
5. Integration Provision or Entire Agreement
The SPACA and this Exhibit A thereto sets forth and constitutes the entire Agreement and understanding of all of the Collaborators with respect to the subject matter hereof. The SPACA and this Exhibit A thereto supersedes any and all prior Agreements, negotiations, correspondence, undertakings, promises, covenants, arrangements, communications, representations, and warranties, whether oral or written, of any Collaborator Party to this Agreement.
6. Limit of Liability
In no event shall any Collaborator be liable to any other Collaborator Party or any third Party in contract, tort or otherwise for incidental or consequential damages of any kind, including, without limitation, punitive or economic damages or lost profits or Intellectual Property infringement or violation, regardless of whether the Collaborator shall be advised, shall have other reason to know or in fact shall know of the possibility. In no event shall any Collaborator be liable for any incidental or consequential damages.
7. Notices
All notices shall be provided to Collaborators by their normal method of direct contact.
8. Severability
If any provision of this Agreement shall be declared by any court of competent jurisdiction to be illegal, void, or unenforceable, the other provisions shall not be affected but shall remain in full force and effect.
9. Written Modification
This Agreement may be amended or modified only by a writing executed by all Parties.
10. Counterparts
This Agreement will be executed in counterparts and Collaborator’s signature on at least one counterpart shall make the Agreement effective for the signing Collaborator.
11. Mediation and Arbitration  
**All disputes arising out of or related to this Agreement shall first be subject to Mediation between all Collaborators. In the event such dispute cannot be resolved through this Mediation process within Ten (10) days after a notice of dispute is given, then binding Arbitration shall be filed in Los Angeles, California pursuant to the rules of and utilizing the facilities and personnel of JAMS. In so agreeing, the Collaborators expressly waive their rights, if any, to a trial by a jury or decided by a judge of these claims and further agree that the award of the arbitrator shall be final and binding upon them as though rendered by a court of law and enforceable in any court having jurisdiction over the same.**
